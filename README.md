# Cloud Computing 1
<figure align = "center">
<img src="images/banner1.png" alt="Trulli" width="800" height="500"">
</figure>

## Name
AWS Application Load Balancer Host Header Based Routing with GitLab Flow. 

## Description
Aim is to improve skills in AWS, Terraform and GitLab.  Projects takes ideas from online courses, websites and articles to produce a working simple application.  it utilises:
- AWS Application Load Balancers, VPCs, Security Groups, Certificate Manager and Route 53
- Creating AWS Architectures in Terraform
- GitLab - CI/CD, Pipelines, Workspaces, AWS Integration and GitLab Flow. 

This project produces a AWS architecture (shown in the  figure below) to execute Host Header Based Routing.  It utilises:
- Application Load Balancer (ALB)
- Elastic Cloud Compute (EC2)
- Virtual Private Cloud (VPC)
- Subnets
- Gateways
- Simple Storage Service (S3)
- Route53
- Identity and Access Management (IAM)
- AWS Certificate Manager (ACM)

The pipeline is defined in the ci-cd.yml defined in the project.  This serves as a basic starter with placeholders for future work such as pushing and pulling for Elastic Container Registry (ECR).  

The project is mostly based on a Terraform Module based course, given in the links below.  I stripped the modules down to resource based Terraform in order to learn the basics of Terraform.    

## Visuals

<figure align = "center">
<img src="images/ALB_v2.png" alt="Trulli" width="800" height="732"">
<figcaption align = "center"><b>Fig.1 - AWS Architecture</b></figcaption>
</figure>



This project demonstrates:
- Infrastructure as code in Terraform
- Design an architecture that is scalable, secure, resilient and facilitates zero downtime deployments
- Setup professional development workflows based on GitLab Flow to automate processes and speed up deployments
- Properly manage and administer an AWS account in accordance with best practices

GitLab Flow is used to produce CI/CD Pipeline based on main, staging and production branches.  Work is carried out locally and then pushed to GitLab flow in order for pipelines to run.  The worklflow is demonstrated below:

<figure align = "center">
<img src="images/Form1-02.png" alt="Trulli" width="600" height="600"">
<figcaption align = "center"><b>Fig.2 - Pipeline Structure</b></figcaption>
</figure>

## Usage
The project utilises Terraform and Gitlab to produce Host Header based routing for a simple app. Security credentials to access AWS from GitLab pipelines are embedded into GitLab variables. This [json file](https://gitlab.com/falmouth158/cars-api/-/snippets/2302746) provides a policy to allow GitLab Pipleines access to AWS resources.   

Using my purchased AWS domain the app is displayed on the following addresses:
```
1. Fixed Response: http://myapps.cloudrichard.com   
2. App1 Landing Page: http://app1.cloudrichard.com
3. App2 Landing Page: http://app2.cloudrichard.com
```
These pages are no longer running on AWS.  In order to reproduce purchase a domain on AWS and clone the project. 

## Contributing
This project utilises the following resources:
- [DevOps](https://www.udemy.com/course/devops-deployment-automation-terraform-aws-docker/)
- [Terraform](https://www.udemy.com/course/terraform-on-aws-with-sre-iac-devops-real-world-demos/)
- [GitLab](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/)


## Project status
