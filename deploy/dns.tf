#DNS Configuration
data "aws_route53_zone" "dns" {
  name = var.dns_name
}
#Record in hosted zone for ACM Certificate Domain verification
resource "aws_route53_record" "cert_validation" {
  for_each = {
    for val in aws_acm_certificate.app-lb-https.domain_validation_options : val.domain_name => {
      name   = val.resource_record_name
      record = val.resource_record_value
      type   = val.resource_record_type
    }
  }
  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.dns.zone_id

}
#Create Alias record towards ALB from Route53
resource "aws_route53_record" "app1" {
  zone_id = data.aws_route53_zone.dns.zone_id
  name    = join(".", ["app1", data.aws_route53_zone.dns.name])
  type    = "A"
  alias {
    name                   = aws_lb.application-lb.dns_name
    zone_id                = aws_lb.application-lb.zone_id
    evaluate_target_health = true
  }
}

#Create Alias record towards ALB from Route53
resource "aws_route53_record" "app2" {
  zone_id = data.aws_route53_zone.dns.zone_id
  name    = join(".", ["app2", data.aws_route53_zone.dns.name])
  type    = "A"
  alias {
    name                   = aws_lb.application-lb.dns_name
    zone_id                = aws_lb.application-lb.zone_id
    evaluate_target_health = true
  }
}
resource "aws_route53_record" "myapps" {
  zone_id = data.aws_route53_zone.dns.zone_id
  name    = join(".", ["myapps", data.aws_route53_zone.dns.name])
  type    = "A"
  alias {
    name                   = aws_lb.application-lb.dns_name
    zone_id                = aws_lb.application-lb.zone_id
    evaluate_target_health = true
  }
}
