variable "aws_region" {
  default = "us-east-1"
}
variable "vpc_cidr" {
  default = "10.20.0.0/16"
}
variable "subnets_cidr" {
  type    = list(any)
  default = ["10.20.1.0/24", "10.20.2.0/24"]
}
variable "azs" {
  type    = list(any)
  default = ["us-east-1a", "us-east-1b"]
}
variable "instance_type" {
  default = "t2.nano"
}
variable "user_data_script" {
  description = "create scripts for different instances"
  type        = list(string)
  default     = ["install_httpd1.sh", "install_httpd2.sh"]
}

variable "dns_name" {
  default = "cloudrichard.com"
}
variable "prefix" {
  default = "terra1"
}
variable "project" {
  default = "aws-terraform-1"
}
variable "contact" {
  default = "email@maintainer.com"
}


