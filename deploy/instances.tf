data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}

resource "aws_instance" "ec2" {
  count           = length(var.subnets_cidr)
  ami             = data.aws_ami.amazon_linux.id
  instance_type   = var.instance_type
  security_groups = [aws_security_group.ec2.id, aws_security_group.alb.id]
  subnet_id       = element(aws_subnet.public.*.id, count.index)
  user_data       = file(element(var.user_data_script, count.index))

  tags = merge(
    local.common_tags,
    tomap({ Name = "instance-${count.index + 1}" })
  )


  #tags = { Name = "instance-${count.index + 1}" }


}

