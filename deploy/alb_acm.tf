#ACM CONFIGURATION
#Creates ACM issues certificate and requests validation via DNS(Route53)
resource "aws_acm_certificate" "app-lb-https" {
  domain_name = "*.${var.dns_name}"
  lifecycle {
    create_before_destroy = true
  }

  validation_method = "DNS"
  tags = {
    Name = "app-ACM"
  }
}
#Validates ACM issued certificate via Route53
resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.app-lb-https.arn
  for_each                = aws_route53_record.cert_validation
  validation_record_fqdns = [aws_route53_record.cert_validation[each.key].fqdn]
}
####ACM CONFIG END


resource "aws_lb" "application-lb" {
  name               = "app-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb.id]
  subnets            = [for subnet in aws_subnet.public : subnet.id]
  tags = {
    Name = "app-LB"
  }
}
resource "aws_lb_listener" "listener_https" {
  load_balancer_arn = aws_lb.application-lb.arn
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = aws_acm_certificate.app-lb-https.arn
  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Fixed response content"
      status_code  = "200"
    }
  }
}

resource "aws_lb_listener" "listener_http" {
  load_balancer_arn = aws_lb.application-lb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
resource "aws_lb_target_group" "app-lb-tg-group" {
  count       = length(var.subnets_cidr)
  name        = "app-lb-tg-${count.index + 1}"
  port        = 80
  target_type = "instance"
  vpc_id      = aws_vpc.terra_vpc.id
  protocol    = "HTTP"
  health_check {
    enabled  = true
    interval = 10
    path     = "/"
    port     = 80
    protocol = "HTTP"
    matcher  = "200-299"
  }
  tags = {
    Name = "app-target-group-${count.index + 1}"
  }
}


resource "aws_alb_target_group_attachment" "main" {
  count            = length(var.subnets_cidr)
  target_group_arn = element(aws_lb_target_group.app-lb-tg-group.*.arn, count.index)
  target_id        = element(aws_instance.ec2.*.id, count.index)
}

resource "aws_lb_listener_rule" "app1" {
  listener_arn = aws_lb_listener.listener_https.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app-lb-tg-group[0].arn
  }

  condition {
    host_header {
      values = ["app1.cloudrichard.com"]
    }
  }
}

resource "aws_lb_listener_rule" "app2" {
  listener_arn = aws_lb_listener.listener_https.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app-lb-tg-group[1].arn
  }

  condition {
    host_header {
      values = ["app2.cloudrichard.com"]
    }
  }
}

